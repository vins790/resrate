/**
 * Created by Maciej on 02.03.2018.
 */
import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {Icon} from 'react-materialize'

class Stars extends Component {

    constructor(props){
        super(props);
        this.state = {
            "tempRate": -1,
            "rate": -1
        }
    }

    componentWillMount(){
        this.setState({
            "tempRate": this.props.stars-1,
            "rate": this.props.stars-1,
            "disable":this.props.disable
        })
    }

    render(){

        let stars = [...new Array(5)].map((star, rate)=>{
           return <span
               key={rate}
               className={this.props.disable?"common--allElements_defaultCoursor":"common--allElements_pointerOnHover"}
               onMouseOver={(e)=>{
                   if(!this.props.disable)
                    this.setState({
                        "tempRate":this.state.rate,
                        "rate":rate
                    })
               }}
               onMouseOut={()=>{
                   if(!this.props.disable)
                   this.setState({
                       "rate": this.state.tempRate
                   })
               }}
               onClick={()=>{
                   if(!this.props.disable) {
                       this.setState({
                           "tempRate": rate,
                           "rate": rate
                       });
                       this.props.returnStars(rate + 1);
                   }
               }}
           >
                   <Icon medium className="star--Icon">{this.state.rate >= rate?"star":"star_border"}</Icon>
           </span>
        });

        return (
            <div className="common--allElements_center">
                {stars}
            </div>
        )
    }
}

Stars.propTypes = {
    returnStars: PropTypes.func.isRequired,
    stars: PropTypes.number.isRequired,
    disable: PropTypes.bool
};


export default Stars;