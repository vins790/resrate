/**
 * Created by Maciej on 02.03.2018.
 */


import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';

import {Row,Col, Button} from 'react-materialize';

import Stars from '../components/stars';
import {setStars} from '../actions/reviewFormActions';
import {setReview} from '../actions/reviewFormActions'

class RevievForm extends Component{

    render(){

        return(
            <div>
                <Row>
                <Stars returnStars={this.props.setStars} stars={0} />
                </Row>
                <Row>
                    <Col s={10} m={10} l={10} offset="s1 m1 l1">
                        <textarea className="reviewForm--textarea" placeholder="Miejsce na Twoją opinię ..." rows="10" />
                    </Col>
                </Row>
                <Row className="common--allElements_center">
                    <Button waves='light'>Wyślij</Button>
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        stars: state.reviewReducer.stars
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setStars:setStars,
        setReview:setReview
    },dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(RevievForm);