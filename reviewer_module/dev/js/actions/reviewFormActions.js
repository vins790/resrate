/**
 * Created by Maciej on 02.03.2018.
 */

export const setStars = (stars) => {
    return {
        type:"SET_STARS",
        payload: stars
    }
};

export const setReview = (review) => {
    return {
        type:"SET_REVIEW",
        payload: review
    }
};