/**
 * Created by Maciej on 02.03.2018.
 */
import {combineReducers} from 'redux';
import ReviewReducer from './reviewReducer';

const allReducers = combineReducers({
    reviewReducer:ReviewReducer
});

export default allReducers;