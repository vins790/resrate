import React, { Component } from 'react';
import ReviewForm from '../containers/reviewForm';

class App extends Component {

  render() {

      function $_GET(param) {
          let  vars = {};
          window.location.href.replace( location.hash, '' ).replace(
              /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
              function( m, key, value ) { // callback
                  vars[key] = value !== undefined ? value : '';
              }
          );

          if ( param ) {
              return vars[param] ? vars[param] : null;
          }
          return vars;
      }

      console.log("location hash: ",window.location.href);

      let GET = $_GET();

      let id = GET['id'];
      let code = GET['code'];

      console.log("ID : ",id);
      console.log("CODE : ",code);

  return (
      <div className="App">
        <h1 className="common--allElements_center" style={{"color":"white", "font-weight":"bold"}}>RESRATE</h1>
        <ReviewForm/>
      </div>
    );
  }
}

export default App;
