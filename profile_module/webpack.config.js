/**
 * Created by Maciej on 05.03.2018.
 */
const path = require('path');
var webpack = require('webpack');

module.exports = {
    devServer: {
        inline: true,
        contentBase: './src',
        port: 3000,
        historyApiFallback: true
    },
    entry:path.resolve(__dirname, 'dev')+'/js/index.js',
    output:{
        path: path.resolve(__dirname, 'src'),
        filename: 'js/ResrateProfileModule.min.js'
    },
    module:{
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_module)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react','es2015']
                }
            },
            {
                test:/\.css$/,
                loader:'style-loader!css-loader'
            },
            {
                test:/\.scss$/,
                loader:'style-loader!css-loader!sass-loader'
            },

        ]
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin()

    ]
};