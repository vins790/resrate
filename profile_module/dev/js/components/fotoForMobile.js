/**
 * Created by Maciej on 05.03.2018.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {Row, Icon} from 'react-materialize'

class FotoForMobile extends Component {

    render(){
        return (
            <Row>
                <Row className="profile--foto">
                    <div className="profile--logo__mobile common--allElements_center">
                        <Icon medium>photo</Icon>
                    </div>
                    <span className="profile--backgroundFoto__mobile">
                        <Icon large>photo_size_select_actual</Icon>
                    </span>
                </Row>
            </Row>
        )
    }
}

FotoForMobile.propTypes = {
};


export default FotoForMobile;




