/**
 * Created by Maciej on 05.03.2018.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {Row, Icon} from 'react-materialize'

class FotoForDesktop extends Component {

    render(){
        return (
            <Row>
                <Row className="profile--foto">
                    <div className="profile--logo common--allElements_center">
                        <Icon medium>photo</Icon>
                        <p>Drop logo Your here</p>
                    </div>
                    <span className="common--allElements_center">
                        <Icon large>photo_size_select_actual</Icon>
                        <p>Drop Your background foto here</p>
                    </span>
                </Row>
            </Row>
        )
    }
}

FotoForDesktop.propTypes = {
};


export default FotoForDesktop;




