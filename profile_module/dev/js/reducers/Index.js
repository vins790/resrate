/**
 * Created by Maciej on 05.03.2018.
 */
import {combineReducers} from 'redux';
import ProfileReducer from './profileReducer';
const allReducers = combineReducers({
    profileReducer:ProfileReducer
});

export default allReducers