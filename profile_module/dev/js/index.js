/**
 * Created by Maciej on 05.03.2018.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './main/App';
import {createStore, applyMiddleware} from "redux";
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import {createLogger} from 'redux-logger';
import allReducers from './reducers';
require('../scss/style.scss');

const store = createStore(allReducers, {}, applyMiddleware(createLogger(), promise(), thunk));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
