const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const ref = admin.database().ref();

exports.deleteInactiveCodes= functions.https.onRequest((request, response) => {
        const currentTime = new Date().getTime();
        const validTime = 2700000;
        const endAtTime = currentTime - validTime;

        var codes = [];

        ref.child('codes').orderByChild('creationTime').endAt(endAtTime).once('value').then(snap=>{
              Object.keys(snap.val()).map((key)=>{
                ref.child('codes/'+key).remove()
              });
                response.send("REMOVED");
        });
});

exports.addCode= functions.https.onRequest((request, response) => {
        var newCode = ref.child('codes').push();
        newCode.set({
            creationTime:1233445
        });
        response.send("ADDED");
});

